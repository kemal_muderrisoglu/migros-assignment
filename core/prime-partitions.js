var total = 0;

exports.calculate = function (n) {
    var fib = fibonacciSequence(n + 3);
    return [
        partition(fib[n - 1]),
        partition(fib[n]),
        partition(fib[n + 1]),
        partition(fib[n + 2])
    ];
};

/**
 * Calculates the prime partition of a given number
 */

function partition(n) {
    total = 0;
    prime_partitions(n, n, 1);
    return total;
}

function prime_partitions(n, max, result) {
    if (n === 0) {
        return total += result;
    }

    for (var i = Math.min(max, n); i >= 1; i--) {
        if (isPrime(i)) {
            prime_partitions(n - i, i, result * i);
        }
    }
}

/**
 * Generates fibionacci sequence list up to a given number
 */
function fibonacciSequence(number) {
    var sequence = [1, 1];
    for (var i = 2; i < number; i++) {
        sequence[i] = sequence[i - 1] + sequence[i - 2];
    }
    return sequence;
}

/**
 * Checks if the given number is prime
 */
function isPrime(num) {
    for (var i = 2; i < num; i++)
        if (num % i === 0)
            return false;
    return num !== 1;
}
