var express = require('express');
var router = express.Router();
var prime = require('../core/prime-partitions');

router.get('/sum/:sum', function (req, res) {
    var sum = req.params.sum;
    if (isNaN(sum)) {
        res.send({input: sum, error: sum + ' is not a number. Please enter a number.'});
    }
    else if (sum < 3) {
        res.send({input: sum, error: 'You should enter a number above 2'});
    }
    else if (sum > 10) {
        res.send({input: sum, error: 'You should enter a number below 10 for memory allocation'});
    }
    else {
        res.status(200).send({input: sum, result: prime.calculate(parseInt(sum))});
    }
});

router.get('*', function (req, res) {
    res.redirect('/sum/3');
});

module.exports = router;